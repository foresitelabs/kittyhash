import os


def crack_md5(input_file, hash_file):
    print("hit")
    os.system("hashcat -a 0 -m 0 {0} {1}".format(hash_file, input_file))
    print("MD5 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_sha1(input_file, hash_file):
    os.system("hashcat -a 0 -m 100 {0} {1}".format(hash_file, input_file))
    print("sha1 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_sha224(input_file, hash_file):
    os.system("hashcat -a 0 -m 1300 {0} {1}".format(hash_file, input_file))
    print("SHA-224 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_sha256(input_file, hash_file):
    os.system("hashcat -a 0 -m 1400 {0} {1}".format(hash_file, input_file))
    print("SHA-256 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_sha384(input_file, hash_file):
    os.system("hashcat -a 0 -m 10800 {0} {1}".format(hash_file, input_file))
    print("SHA-384 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_sha512(input_file, hash_file):
    os.system("hashcat -a 0 -m 1700 {0} {1}".format(hash_file, input_file))
    print("SHA-512 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

## need to implement
def crack_blake2b(input_file, hash_file):
    os.system("hashcat -a 0 -m 600 {0} {1}".format(hash_file, input_file))
    print("SHA-224 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

### need to implement
# def crack_blake2s(input_file, hash_file):
#     os.system("hashcat -a 0 -m 1300 {0} {1}".format(hash_file, input_file))
#     print("SHA-224 hashes cracked.")
#     print("Deleting Hash Files...")
#     os.system("rm -f {0}".format(hash_file))

def crack_sha3_224(input_file, hash_file):
    os.system("hashcat -a 0 -m 17300 {0} {1}".format(hash_file, input_file))
    print("SHA3-224 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_sha3_256(input_file, hash_file):
    os.system("hashcat -a 0 -m 17400 {0} {1}".format(hash_file, input_file))
    print("SHA3-256 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_sha3_512(input_file, hash_file):
    os.system("hashcat -a 0 -m 17300 {0} {1}".format(hash_file, input_file))
    print("SHA3-224 hashes cracked.")
    print("Deleting Hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_ntlm(input_file, hash_file):
    os.system("hashcat -a 0 -m 1000 {0} {1}".format(hash_file, input_file))
    print("NTLM hashes Cracked")
    print("Deleting hash Files...")
    os.system("rm -f {0}".format(hash_file))

def crack_ntlmV2(input_file, hash_file):
    os.system("hashcat -a 0 -m 0 {0} {1}".format(hash_file, input_file))
    print("NTLMv2 hashes Cracked")
    print("Deleting hash Files...")
    os.system("rm -f {0}".format(hash_file))
